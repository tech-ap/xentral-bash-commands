# Bash toolset for AWS spot based setup for Xentral

## Requerements

+ Please install AWS-CLI
+ Please configure AWS-CLI

**~/.aws/config**

```
[profile X]
region = eu-central-1
```

**~/.aws/credentials**

```
[X]
aws_access_key_id = your access key id 
aws_secret_access_key = your secret access key
```

+ please clone this repository to your home directory
+ please clone **git@bitbucket.org:tech-ap/bash-aws.git** to your home directory
+ please add following lines to **~/.profile**

```
source ~/bash-aws/bashrc
for d in *-bash-commands; do source ~/$d/bashrc; done
```

+ please either restart your terminal or call
```source ~/.profile```

## Available commands

```X-bronze```

 Connects to all web facing servers via csshX (check and install csshX if needed) 

```xentral-CC```

'*xentral-CC bagrar user*' will connect to the bagrar cloud  server as user "user" 

